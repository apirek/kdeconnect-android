El KDE Connect proporciona un conjunt de característiques per a integrar el flux de treball entre dispositius:

- Porta-retalls compartit: copieu i enganxeu entre els dispositius.
- Compartiu fitxers i URL a l'ordinador des de qualsevol aplicació.
- Obteniu notificacions de trucades entrants i missatges SMS al vostre PC.
- Ratolí tàctil virtual: utilitzeu la pantalla del telèfon com a ratolí tàctil de l'ordinador.
- Sincronització de notificacions: llegiu les notificacions de l'Android a l'escriptori.
- Control remot multimèdia: utilitzeu el telèfon com a control remot dels reproductors multimèdia Linux.
- Connexió Wi-Fi: no cal connexió USB ni Bluetooth.
- Encriptatge TLS d'extrem a extrem: la informació és segura.

Tingueu en compte que haureu d'instal·lar el KDE Connect a l'ordinador perquè aquesta aplicació funcioni, i mantingueu actualitzada la versió d'escriptori amb la versió de l'Android perquè funcionin les últimes característiques.

Informació dels permisos confidencials:
* Permís d'accessibilitat: requerit per rebre l'entrada des d'un altre dispositiu per controlar el telèfon Android, si utilitzeu la característica d'entrada remota.
* Permís d'ubicació en segon pla: requerit per saber a quina xarxa WiFi esteu connectat, si utilitzeu la característica xarxes de confiança.

El KDE Connect mai envia cap informació a KDE ni a cap tercer. El KDE Connect envia dades des d'un dispositiu a l'altre directament utilitzant la xarxa local, mai a través d'Internet, i utilitzant l'encriptatge d'extrem a extrem.

Aquesta aplicació forma part d'un projecte de codi obert i existeix gràcies a totes les persones que hi han contribuït. Visiteu el lloc web per aconseguir el codi font.